import Vue from 'vue'
import Router from 'vue-router'
import tenderAnno from '@/views/tenderAnno.vue'
import bidAnno from '@/views/bidAnno.vue'
import changeNotice from '@/views/changeNotice.vue';
import NoticeDetail from '@/views/NoticeDetail.vue'
import TenderDetail from '@/views/TenderDetail.vue';
import BidDetail from '@/views/BidDetail.vue'
import IndustryNews from '@/views/IndustryNews.vue'
Vue.use(Router)

let kejianrouter = new Router({
  routes: [{
    path: '/',
    name: 'home',
    component: () => import('./views/Home.vue')
  },
  {
    path: '/news',
    name: 'news',
    component: () => import('./views/News.vue'),
  },
  {
    path: '/industry-news',
    name: 'industry-news',
    component: IndustryNews
  },
  {
    path: '/newsdetails/:id',
    name: 'newsdetails',
    component: () => import('./views/NewsDetails.vue'),
  },
  {
    path: '/product',
    name: 'product',
    component: () => import('./views/Product.vue'),
  },
  {
    path: '/case',
    name: 'case',
    component: () => import('./views/Case.vue')
  },
  {
    path: '/casedetails/:id',
    name: 'casedetails',
    component: () => import('./views/CaseDetails.vue')
  },
  {
    path: '/goin',
    name: 'goin',
    component: () => import('./views/GoIn.vue')
  },
  {
    path: '/download',
    name: 'download',
    component: () => import('./views/Download.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('./views/Login.vue')
  },
  {
    path: '/tender-anno',
    component: tenderAnno
  },
  {
    path: '/change-notice/:id',
    name: 'change-notice-detail',
    component: NoticeDetail
  },
  {
    path: '/bid-anno/:id',
    name: 'bid-anno-detail',
    component: BidDetail
  },
  {
    path: '/tender-anno/:id',
    name: 'tender-anno-detail',
    component: TenderDetail
  },
  {
    path: '/bid-anno',
    component: bidAnno
  },
  {
    path: '/change-notice',
    component: changeNotice
  },
  {
    path: '/admin',
    name: 'admin',
    meta: {
      requireAuth: true
    },
    component: () => import('./views/Admin.vue'),
    children: [{
      path: '/admin/user',
      name: 'user',
      component: () => import('./views/Admin/User.vue')
    },
    {
      path: '/admin/news',
      name: 'new',
      component: () => import('./views/Admin/News.vue')
    },
    {
      path: '/admin/cases',
      name: 'cases',
      component: () => import('./views/Admin/Cases.vue')
    },
    {
      path: '/admin/team',
      name: 'team',
      component: () => import('./views/Admin/Team.vue')
    },
    {
      path: '/admin/course',
      name: 'course',
      component: () => import('./views/Admin/Course.vue')
    },
    {
      path: '/admin/enterprise',
      name: 'enterprise',
      component: () => import('./views/Admin/Enterprise.vue')
    },
    {
      path: '/admin/honor',
      name: 'honor',
      component: () => import('./views/Admin/Honor.vue')
    },
    {
      path: '/admin/dictionary',
      name: 'dictionary',
      component: () => import('./views/Admin/Dictionary.vue')
    },
    {
      path: '/admin/page',
      name: 'page',
      component: () => import('./views/Admin/Page.vue')
    }
    ]
  }
  ]
})

// 判断是否需要登录权限 以及是否登录
kejianrouter.beforeEach((to, from, next) => {
  // 判断是否需要登录权限
  if (to.matched.some(res => res.meta.requireAuth)) {
    // 判断是否登录
    if (sessionStorage.getItem('token')) {
      next()
    } else {
      // 没登录则跳转到登录界面
      next({
        path: '/login',
        query: {
          redirect: to.fullPath
        }
      })
    }
  } else {
    next()
  }
})

export default kejianrouter
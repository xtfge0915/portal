const fs = require('fs');
const path = require('path');
const download = require('download');
const baseUrl = 'http://shkjgw.shkjem.com';
function downloadFile(url) {
  const dirname = path.dirname(url);
  const basename = path.basename(url);
  download(baseUrl + url, path.join(__dirname, 'public', dirname), {filename: basename})
}
[
  {
      "Id": 16,
      "Img": "/imagestore/2019/0425/ad5bffa4-e608-4f0c-8cbd-f9daa2af8ef2.jpg",
      "Remark": "参加第七届中国（上海）国际技术进出口交易会",
      "CreateTime": "2023-11-29T13:50:15.743"
  },
  {
      "Id": 11,
      "Img": "/imagestore/2019/0425/9db92c6e-95e9-43df-b6d4-8b2e83f85dbe.jpg",
      "Remark": "参加第七届中国（上海）国际技术进出口交易会",
      "CreateTime": "2020-06-05T21:34:41.023"
  },
  {
      "Id": 6,
      "Img": "/imagestore/2019/0425/3ee5db59-d021-4d0d-927e-70e850303f16.jpg",
      "Remark": "参加第七届中国（上海）国际技术进出口交易会",
      "CreateTime": "2020-06-05T21:34:36.397"
  },
  {
      "Id": 22,
      "Img": "/imagestore/2019/0426/cafb7b6a-71dd-4a9b-881b-d3f9da6b8967.jpg",
      "Remark": "",
      "CreateTime": "2019-04-26T16:06:18.193"
  },
  {
      "Id": 21,
      "Img": "/imagestore/2019/0426/958585c0-18a3-450b-b7e1-c8ee4c2f7bec.jpg",
      "Remark": "",
      "CreateTime": "2019-04-26T16:06:11.277"
  },
  {
      "Id": 20,
      "Img": "/imagestore/2019/0426/37af22f2-d6d8-4b10-8377-9a28fa8dd8db.jpg",
      "Remark": "",
      "CreateTime": "2019-04-26T16:06:03.21"
  },
  {
      "Id": 19,
      "Img": "/imagestore/2019/0426/f40c7512-4057-4b2f-84dc-d8ee93d6f5e1.jpg",
      "Remark": "",
      "CreateTime": "2019-04-26T16:05:55.947"
  },
  {
      "Id": 18,
      "Img": "/imagestore/2019/0426/f929d681-38c3-4d4f-b845-fa695f4801e8.jpg",
      "Remark": "",
      "CreateTime": "2019-04-26T16:05:47.69"
  },
  {
      "Id": 17,
      "Img": "/imagestore/2019/0426/12633c2f-387c-4587-a88a-43666265f886.jpg",
      "Remark": "",
      "CreateTime": "2019-04-26T16:05:38.187"
  }
].forEach(item => {
  downloadFile(item.Img);
})